---
title: post title with whitespace
date: 2022-01-27 16:17:52
tags:
---

# docker镜像设置时区



alpine系统：

​		exec -it 进入容器设置

1. `apk add -U tzdata`
2. `cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime`

ubuntu系统: 

​		docker-compose里设置环境变量

1. ```
   environment:  TZ: Asia/Shanghai
   ```

